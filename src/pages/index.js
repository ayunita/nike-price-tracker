import React from "react"
import { Router } from "@reach/router"
import Navigator from "@components/navigator"
import Home from "@components/home"
import Shoes from "@components/shoes"
import Footer from "@components/footer"
import SEO from "../components/seo"

const IndexPage = () => (
  <>
    <SEO title="Home" />
    <Navigator />
    <main>
    <Router>
      <Home path="/" />
      <Shoes path="/shoes/:brand/:gender/:title" />
    </Router>
    </main>
    <Footer />
  </>
)

export default IndexPage
