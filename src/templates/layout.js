import React from "react"
import styled from "styled-components"

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

const Content = styled.div`
  margin-top: 72px;
  width: 100%;
  min-height: 100vh;
  padding: 0 12%;

  @media(max-width: 768px) {
    padding: 0;
  }
`
const Layout = ({ children }) => (
  <Container>
    <Content>{children}</Content>
  </Container>
)

export default Layout
