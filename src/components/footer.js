import React from "react"

const Footer = () => (
  <footer>
    <span>Nike sustainable shoes price tracker</span>
    <span>This site used data from official Nike Store and only captured "Sustainable Materials" marked shoes.</span>
    <span>
      Built with{" "}
      <span role="img" aria-label="emoji love feet, money, planet">
        💖🦶💰🌎
      </span>{" "}
      | Visit me{" "}
      <a target="_blank" rel="noreferrer noopener" href="https://ayunita.xyz/">
        here
      </a>
    </span>
  </footer>
)

export default Footer
