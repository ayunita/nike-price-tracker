import React from "react"
import styled from "styled-components"

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
`

const ColorButton = styled.button`
  background: ${props => props.color};
`
const GENDER_DATA = [
  {
    value: "all",
    name: "All",
  },
  {
    value: "woman",
    name: "Women",
  },
  {
    value: "man",
    name: "Men",
  },
  {
    value: "unisex",
    name: "Unisex",
  },
]

const COLOR_DATA = [
  {
    value: "white",
    name: "#ffffff",
  },
  {
    value: "platinum",
    name: "#e5e4e2",
  },
  {
    value: "grey",
    name: "#8c8c8c",
  },
  {
    value: "black",
    name: "#000000",
  },
  {
    value: "blue",
    name: "#2F77FF",
  },
  {
    value: "red",
    name: "#DB2525",
  },
  {
    value: "green",
    name: "#1ff207",
  },
  {
    value: "yellow",
    name: "#ffe629",
  },
  {
    value: "pink",
    name: "#FEAEFF",
  },
  {
    value: "purple",
    name: "#633EE1",
  },
]

const Filter = ({ value, valueChanged }) => {
  const handleGender = gender => {
    const temp = { ...value, gender: gender }
    valueChanged(temp)
  }

  const handleColor = color => {
    let temp = {
      ...value,
      color: { ...value.color },
    }
    temp.color[color] = !value.color[color]
    valueChanged(temp)
  }

  return (
    <Container>
      {GENDER_DATA.map(b => (
        <button
          key={b.value}
          className={`button default ${
            b.value === value.gender ? "active" : ""
          }`}
          onClick={() => handleGender(b.value)}
        >
          {b.name}
        </button>
      ))}
      <span style={{ margin: "8px 0" }}>
        {COLOR_DATA.map(b => (
          <ColorButton
            key={b.value}
            className={`button color ${value.color[b.value] ? "" : "exclude"}`}
            color={b.name}
            onClick={() => handleColor(b.value)}
          ></ColorButton>
        ))}
      </span>
    </Container>
  )
}

export default Filter
