import React, { useState, useEffect } from "react"
import {
  LineChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Line,
  ResponsiveContainer,
} from "recharts"
import moment from "moment"

const formatDate = (tick) => {
    return moment(new Date(tick)).format("DD-MMM-YY");
}

const Chart = ({ data }) => {
  const [prices, setPrices] = useState(data)
  useEffect(() => {
    const getDaysDiff = (date1, date2) => {
      const dt1 = new Date(date1)
      const dt2 = new Date(date2)
      return Math.floor(
        (Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) -
          Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) /
          (1000 * 60 * 60 * 24)
      )
    }

    // add data between date
    let temp = []
    for (let i = 0; i < data.length; i++) {
      const current_date = new Date(data[i].date)
      let next_date = new Date()
      if (data[i + 1]) {
        next_date = new Date(data[i + 1].date)
      }
      const difference = getDaysDiff(current_date, next_date)
      for (let j = 0; j < difference; j++) {
        const d = new Date(+current_date)
        d.setDate(d.getDate() + j)

        const x = {
          "nike.ca": data[i].price,
          date: d.getTime(),
        }
        temp.push(x)
      }
    }
    // add last date
    const x = {
      "nike.ca": data[data.length - 1].price,
      date: new Date().getTime(),
    }
    temp.push(x)

    setPrices(temp)
  }, [data])

  return (
    <div className="card-table">
      <div className="card-table-header">Price History</div>
      <ResponsiveContainer width="100%" height={250}>
        <LineChart
          data={prices}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="date" tickFormatter={formatDate} hasTick tick={{fontSize: 10}} />
          <YAxis />
          <Tooltip labelFormatter={formatDate}/>
          <Legend />
          <Line type="monotone" dataKey="nike.ca" stroke="#82ca9d" />
        </LineChart>
      </ResponsiveContainer>
    </div>
  )
}

export default Chart
