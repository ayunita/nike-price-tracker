import React from "react"
import Card from "@components/card"
import styled from "styled-components"
import slugify from "slugify"

const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(4, minmax(0, 1fr));
  min-height: 300px;

  @media (max-width: 768px) {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
`

const Grid = ({ sectionTitle, footwears, isLoading, isError, filter }) => {
  return (
    <section
      id={slugify(sectionTitle, {
        replacement: "-",
        lower: true,
        remove: /[*+~.()'"!:@]/g,
      })}
    >
      <div className="title">
        <span>{sectionTitle}</span>
      </div>
      {filter}
      <Container>
        {isLoading ? <div style={{ margin: "auto" }}>Loading...</div> : null}
        {isError ? (
          <div style={{ margin: "center" }}>
            Error while fetching the data :(
          </div>
        ) : null}
        {footwears.map((item, index) => {
          const made_from = item.description.split(/made from/gi)[1]
          let made_from_text = ""
          if (made_from) made_from_text = `Made from ${made_from.split(".")[0]}`
          else made_from_text = item.description
          let lowest = false
          if (
            item.price.currentPrice === item.lowest_price &&
            item.price.currentPrice !== item.price.fullPrice
          )
            lowest = true

          return (
            <Card
              key={`${sectionTitle}${index}`}
              cid={item.cid}
              brand={item.brand}
              gender={item.gender}
              imageUrl={item.image_url}
              title={item.title}
              subtitle={item.subtitle.toUpperCase()}
              color={made_from_text ? made_from_text : ``}
              currentPrice={item.price.currentPrice}
              fullPrice={item.price.fullPrice}
              lowest={lowest}
              discount={item.discount}
              rates={parseFloat(item.rating.rates)}
            />
          )
        })}
      </Container>
    </section>
  )
}

export default Grid
