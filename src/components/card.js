import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import StarRatings from "react-star-ratings"

const Container = styled.div`
  position: relative;
  width: calc(100% - 64px);
  min-height: 300px;
  padding: 16px;
  margin: 16px;
  border-radius: 25px;
  box-shadow: 3px 3px 10px 0 rgba(217, 210, 200, 0.51),
    -3px -3px 10px 0 rgba(255, 255, 255, 0.83);
`

const Detail = styled.div``

const Title = styled.div`
  color: black !important;
`

const Color = styled.div`
  font-size: 0.7rem;
`

const Price = styled.div`
  font-size: 1.3rem;
  margin-top: 8px;
  color: #8900f2;
`

const Image = styled.img`
max-width: 100%;
height: auto;
`

const Discount = styled.div`
  position: absolute;
  top: 16px;
  right: 16px;
  width: 36px;
  height: 36px;
  border-radius: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #f20089;
  font-size: 0.8rem;
  color: white;
  z-index: 9998;
`

const TopBadge = styled.div`
  font-size: 0.8rem;
  text-align: center;
`

const Badge = styled.span`
  font-size: 0.5rem;
  padding: 4px;
  border-radius: 4px;
  background: #00fed8;
  color: #000;
  font-weight: 600;
  text-transform: uppercase;
`

const Card = ({
  brand,
  cid,
  title,
  subtitle,
  gender,
  color,
  imageUrl,
  currentPrice,
  fullPrice,
  lowest,
  rates,
  discount,
}) => {
  return (
    <Container>
      <Link to={`/shoes/${brand}/${gender}/${title}`} state={{ cid: cid }}>
        {discount > 0 ? (
          <Discount>
            <span>{discount}%</span>
            <span style={{ fontSize: ".2rem" }}>off</span>
          </Discount>
        ) : null}
        <TopBadge>{subtitle}</TopBadge>
        <Image
          src={imageUrl}
          className={`img-white-bg ${brand === "adidas" ? "adidas-img" : ""}`}
        />
        <Detail>
          <Title>{title}</Title>
          <Color>{color}</Color>
          {rates > 0 ? (
            <StarRatings
              starDimension="12px"
              starSpacing="0"
              rating={rates}
              starRatedColor="#f20089"
              numberOfStars={5}
              name="rating"
            />
          ) : null}
          <Price>
            {fullPrice === currentPrice ? (
              <>${currentPrice}</>
            ) : (
              <>
                <strike>${fullPrice}</strike> ${currentPrice}
              </>
            )}
          </Price>
          {lowest ? <Badge>Lowest price</Badge> : null}
        </Detail>
      </Link>
    </Container>
  )
}

export default Card
