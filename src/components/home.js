import React, { useState, useEffect } from "react"
import axios from "axios"
import Layout from "@templates/layout"
import Grid from "./grid"
import Filter from "./filter"
import headerImage from "@images/open-doodles-float.svg"

const API_URL = "https://nike-shoes-scrape.herokuapp.com/api/v1/colors/all"
const DEFAULT_FILTER = { gender: "all", color: {
  "white": true,
  "platinum": true,
  "grey": true,
  "black": true,
  "blue": true,
  "red": true,
  "green": true,
  "yellow": true,
  "pink": true,
  "purple": true
} }
const Home = () => {
  const [footwears, setFootwears] = useState([])
  const [popular, setPopular] = useState([])
  const [discount, setDiscount] = useState([])
  const [filter, setFilter] = useState(DEFAULT_FILTER)
  const [filteredFootwears, setFilteredFootwears] = useState([])
  const [isLoadingFw, setIsLoadingFw] = useState(false)
  const [isErrorFw, setIsErrorFw] = useState(false)

  useEffect(() => {
    const fetchColors = async () => {
      setIsErrorFw(false)
      setIsLoadingFw(true)

      try {
        const result = await axios(API_URL)

        const dict = {}
        if (result.data) {
          const cl = result.data.results.filter(
            i =>
              i.brand === "nike" &&
              (i.gender === "woman" ||
                i.gender === "man" ||
                i.gender === "unisex")
          )

          cl.forEach(i => {
            if (dict[`${i.title} ${i.gender}`]) {
              // if price is lower, then update cid, desc, image, price
              if (
                dict[`${i.title} ${i.gender}`].price.currentPrice >
                i.price.currentPrice
              ) {
                dict[`${i.title} ${i.gender}`].cid = i.cid
                dict[`${i.title} ${i.gender}`].colorDescription = i.description
                dict[`${i.title} ${i.gender}`].image_url = i.image_url
                dict[`${i.title} ${i.gender}`].price = i.price
                dict[`${i.title} ${i.gender}`].discount = Math.round(
                  ((i.price.fullPrice - i.price.currentPrice) /
                    i.price.fullPrice) *
                    100
                )
                const x = i.price_logs.reduce((a, b) =>
                  b.price < a.price ? b : a
                ).price

                if (dict[`${i.title} ${i.gender}`].lowest_price > x)
                  dict[`${i.title} ${i.gender}`].lowest_price = x
              }
            } else {
              dict[`${i.title} ${i.gender}`] = {
                brand: i.brand,
                title: i.title,
                subtitle: i.subtitle,
                gender: i.gender,
                description: i.text,
                cid: i.cid,
                image_url: i.image_url,
                colorDescription: i.description,
                rating: i.rating,
                price: i.price,
                discount: Math.round(
                  ((i.price.fullPrice - i.price.currentPrice) /
                    i.price.fullPrice) *
                    100
                ),
                lowest_price: i.price_logs.reduce((a, b) =>
                  b.price < a.price ? b : a
                ).price,
              }
            }
          })

          // filter favorite & discounted
          const fw_popular = Object.values(dict)
            .sort((a, b) => b.rating.rates - a.rating.rates)
            .slice(0, 4)
          const fw_discount = Object.values(dict)
            .filter(r => r.discount > 0)
            .sort((a, b) => b.discount - a.discount)

          setPopular(fw_popular)
          setDiscount(fw_discount)

          // reformat all colors' props, add discount and lowest price
          const format_cl = cl.map(i => ({
            ...i,
            discount: Math.round(
              ((i.price.fullPrice - i.price.currentPrice) / i.price.fullPrice) *
                100
            ),
            lowest_price: dict[`${i.title} ${i.gender}`].lowest_price,
          }))

          setFootwears(format_cl)
          setFilteredFootwears(format_cl)
        }
      } catch (error) {
        setIsErrorFw(true)
      }
      setIsLoadingFw(false)
    }

    fetchColors()
  }, [])

  useEffect(() => {
    if (filter) {
      const { gender, color } = filter
      let filtered = [...footwears]
      if (gender !== "all") filtered = filtered.filter(i => i.gender === gender)

      const selected_colors = Object.keys(color).filter(i => color[i])
      filtered = filtered.filter(i => selected_colors.some(c => i.description.toLowerCase().includes(c)))
      setFilteredFootwears(filtered)
    }
  }, [filter]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <div className="header">
        <div className="header-text">
          <p>Nike sustainable shoes price tracker</p>
          <span>Because we care for</span>
          <br />
          <span>our feet, wallet,</span>
          <br />
          <span>and planet</span>
        </div>
        <img src={headerImage} alt="header" />
      </div>
      <Layout>
        <Grid
          sectionTitle="Special offer"
          footwears={discount}
          isLoading={isLoadingFw}
          isError={isErrorFw}
        />
        <Grid
          sectionTitle="Loved by human"
          footwears={popular}
          isLoading={isLoadingFw}
          isError={isErrorFw}
        />
        <Grid
          sectionTitle="All colors"
          footwears={filteredFootwears}
          isLoading={isLoadingFw}
          isError={isErrorFw}
          filter={<Filter value={filter} valueChanged={setFilter} />}
        />
      </Layout>
    </>
  )
}

export default Home
