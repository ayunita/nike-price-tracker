import React, { useState, useEffect } from "react"
import { useParams } from "@reach/router"
import axios from "axios"
import styled from "styled-components"
import Layout from "@templates/layout"
import Chart from "./chart"

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const ImageWrapper = styled.div`
  flex: 1;
  padding: 16px;
  text-align: center;
`

const TextWrapper = styled.div`
  flex: 1;
  padding: 16px;
`

const OptionButton = styled.button`
  width: 15%;
  height: auto;
  margin: 0 8px;
  background: white;
  border: 1px solid black;
  cursor: pointer;
`

const Shoes = ({ location }) => {
  const params = useParams()
  const { brand, gender, title } = params
  const [colors, setColors] = useState([])
  const [options, setOptions] = useState({})
  const [selected, setSelected] = useState(0)
  const [isLoading, setIsLoading] = useState(false)
  const [isError, setIsError] = useState(false)

  useEffect(() => {
    const fetchColors = async () => {
      setIsError(false)
      setIsLoading(true)

      try {
        const url = `https://nike-shoes-scrape.herokuapp.com/api/v1/colors/${brand}+${title}`
        const result = await axios(url)

        if (result.data) {
          const data = result.data.results.filter(i => i.gender === gender)

          // set colors data
          setColors(data)

          const color_opts = data.map((c, index) => ({
            id: index,
            cid: c.cid,
            image: c.image_url,
          }))

          // set color options
          setOptions(color_opts)

          // set selected color index, the default is index 0
          if (location.state) {
            const new_selected = color_opts.findIndex(i => i.cid === location.state.cid)
            setSelected(new_selected)
          }
        }
      } catch (error) {
        setIsError(true)
      }
      setIsLoading(false)
    }

    fetchColors()
  }, [gender, brand, title, location.state])

  return (
    <Layout>
      {isLoading ? <div style={{ margin: "auto" }}>Loading...</div> : null}
      {isError ? (
        <div style={{ margin: "center" }}>Error while fetching the data :(</div>
      ) : null}
      {colors.length > 0 ? (
        <Container>
          <ImageWrapper>
            <img
              className="img-white-bg"
              src={colors[selected].image_url}
              alt={colors[selected].title}
            />
            {options.length > 1 ? (
              <Container style={{ justifyContent: "center" }}>
                {options.map((item, index) => (
                  <OptionButton key={index} onClick={() => setSelected(index)}>
                    <img
                      className="img-white-bg"
                      src={item.image}
                      alt={item.image}
                      style={{ width: "100%", height: "auto" }}
                    />
                  </OptionButton>
                ))}
              </Container>
            ) : null}
            <div style={{ textAlign: "left", margin: "2rem 0" }}>
              <p style={{ fontSize: "1rem", lineHeight: "1.5rem" }}>
                {colors[selected].text}
              </p>
            </div>
          </ImageWrapper>
          <TextWrapper>
            <span>
              {`${params.brand.toUpperCase()} ${colors[
                selected
              ].subtitle.toUpperCase()}`}
            </span>
            <h2>{params.title.toUpperCase()}</h2>
            <h4>{colors[selected].description}</h4>
            <div>
              <span style={{ marginRight: "32px" }}>
                MSRP: ${colors[selected].price.fullPrice}
              </span>
              <span>Rating: {colors[selected].rating.rates}</span>
            </div>
            <div className="card-table">
              <div className="card-table-header">Current Prices</div>
              <table className="table-price">
                <thead>
                  <tr>
                    <th>Store</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <a
                        target="_blank"
                        rel="noreferrer noopener"
                        href={colors[selected].page_url.replace(
                          "{countryLang}",
                          "https://www.nike.com/ca"
                        )}
                      >
                        nike.ca
                      </a>
                    </td>
                    <td>
                      <span style={{ marginRight: "16px" }}>
                        ${colors[selected].price.currentPrice}
                      </span>
                      {colors[selected].price.discounted ? (
                        <span className="tag-badge">
                          {Math.round(
                            ((colors[selected].price.fullPrice -
                              colors[selected].price.currentPrice) /
                              colors[selected].price.fullPrice) *
                              100
                          )}
                          % off
                        </span>
                      ) : null}
                      {colors[selected].in_stock ? (
                        <span className="tag-badge">In stock</span>
                      ) : (
                        <span className="tag-badge">Out of stock</span>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>footlocker.ca</td>
                    <td>
                      <span>Coming soon</span>
                    </td>
                  </tr>
                  <tr>
                    <td>sportchek.ca</td>
                    <td>
                      <span>Coming soon</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <Chart data={colors[selected].price_logs} />
          </TextWrapper>
        </Container>
      ) : null}
    </Layout>
  )
}

export default Shoes
