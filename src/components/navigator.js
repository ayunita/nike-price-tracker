import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"
import slugify from "slugify"
import { AnchorLink } from "gatsby-plugin-anchor-links"

const Container = styled.nav`
  width: calc(100% - 24%);
  padding: 0 12%;
  height: 64px;
  background: #fff;
  border-bottom: 2px solid #e5e5e5;
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media(max-width: 768px) {
    width: calc(100% - 16px);
    padding: 0 8px;
  }
`

const Title = styled.div`
  text-align: left;
  font-size: 1.5rem;
  font-weight: 600;
  text-transform: uppercase;
`

const Menu = styled.div`
  display: flex;
  justify-content: space-around;
  font-weight: 600;
`

const ANCHORS = ["Special Offer", "Loved by Human", "All Colors"]

const Navigator = () => {
  return (
    <Container>
      <Title>
        <Link className="default-link" to="/">
          sushoesm{" "}
          <span role="img" aria-label="canada">
            🇨🇦
          </span>
        </Link>
      </Title>
      <div style={{ width: "50%" }}>
        <Menu>
          {ANCHORS.map(a => {
            const slug = slugify(a, {
              replacement: "-",
              lower: true,
              remove: /[*+~.()'"!:@]/g,
            })
            return <AnchorLink key={slug} to={`/#${slug}`} title={a} />
          })}
        </Menu>
      </div>
    </Container>
  )
}

export default Navigator
